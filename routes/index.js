module.exports = function (app, addon) {

    // Root route. This route will serve the `atlassian-connect.json` unless the
    // documentation url inside `atlassian-connect.json` is set
    app.get('/', function (req, res) {
        res.format({
            // If the request content-type is text-html, it will decide which to serve up
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            // This logic is here to make sure that the `atlassian-connect.json` is always
            // served up when requested by the host
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });
    // This is an example route that's used by the default "generalPage" module.
    // Verify that the incoming request is authenticated with Atlassian Connect
    app.get('/issue_plugin', addon.authenticate(), function (req, res) {
        // Rendering a template is easy; the `render()` method takes two params: name of template
        // and a json object to pass the context in
        res.render('issue_plugin', {
            title: 'Atlassian Connect'
            //issueId: req.query['issueId']
        });
    }
    );
    // Add any additional route handlers you need for views or REST resources here...
    app.get('/addonData', addon.checkValidToken(), function (req, res) {

        const httpClient = addon.httpClient(req);
        const search = req.query.search;

        console.log(`JQL search: ${search}`);

        const httpClientGet = url => new Promise(
            (resolve, reject) => {
                httpClient.get(
                    {
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },
                        url
                    },
                    (err, response, body) => {
                        body = JSON.parse(body);

                        if (body.errorMessages && body.errorMessages.length) {
                            reject({ status: response.statusCode, errors: body.errorMessages });

                            return;
                        }

                        resolve(body);
                    }
                );
            }
        );

        httpClientGet(`rest/api/2/search?jql=${encodeURIComponent(search)}&maxResults=500`)
            .then(
                body => {
                    console.log('Issues',body.issues[0].fields);

                    console.log(`Issues for '${search}' loaded.`);

                    return (body.issues || []).map(issues => ({
                        issueId: issues.id,
                        nameIssue: issues.fields.summary,
                        priorityIssue: issues.fields.priority.name,
                        statusIssue: issues.fields.issuetype.name,
                        dateCreatedIssue: issues.fields.created,
                        keyProject: issues.key,
                        nameProject: issues.fields.project.name,
                        nameCreator: issues.fields.creator.displayName,
                        emailAddressCreator: issues.fields.creator.emailAddress
                    }));
                },
                ({ status, errors }) => {
                    console.log(`Issues for '${search}' loading errors: ${errors}.`);

                    res.status(status).send({ errors, issues: [] });
                }
            )
            .then(issues => {
                const loadComments = issue =>
                    httpClientGet(`rest/api/2/issue/${issue.issueId}/comment`)
                        .then(
                            body => {
                               

                                console.log('Comments');

                                console.log(`Comments for issue '${issue.issueId}' loaded.`);


                                return (body.comments || []).map(comments => ({
                                    commentId: comments.id,
                                    author: comments.author.name,
                                    body: comments.body
                                }));

                                /*issue.comments = comments;*/

                                 //цепочка comments
                            },
                            ({ errors }) => {
                                console.log(`Comments for issue '${issue.issueId}' loading errors: ${errors}.`);

                                return [];
                            }
                        );
                const updateIssueComments
                    = (issuesAcc, issue) =>
                        loadComments(issue)
                            .then(comments => [...issuesAcc, Object.assign({}, issue, { comments })]);

                const issuesCommentsUpdate$
                    = issues
                        .slice(1)
                        .reduce(
                            (acc$, i) => acc$.then(issuesAcc => updateIssueComments(issuesAcc, i)),
                            updateIssueComments([], issues[0])
                        );

                return issuesCommentsUpdate$;
            })
            .then(issues => {
                const loadChangeLog = issue =>
                    httpClientGet(`rest/api/2/issue/${issue.issueId}/changelog`)
                        .then(
                            body => {
                                
                                console.log('History');

                                console.log(`History for issue '${issue.issueId}' loaded.`);


                                return (body.values || []).map(values => ({
                                    historyId: values.id,
                                    date: values.created,
                                    history: values.items
                                }));
                        
                            },
                            ({ errors }) => {
                                console.log(`History for issue '${issue.issueId}' loading errors: ${errors}.`);

                                issue.histories = [];
                            }
                        );
                const updateIssueChangeLog
                    = (issuesAcc, issue) =>
                        loadChangeLog(issue)
                            .then(histories => [...issuesAcc, Object.assign({}, issue, { histories })]);

                const issuesChangeLogUpdate$
                    = issues
                        .slice(1)
                        .reduce(
                            (acc$, i) => acc$.then(issuesAcc => updateIssueChangeLog(issuesAcc, i)),
                            updateIssueChangeLog([], issues[0])
                        );

                return issuesChangeLogUpdate$;
            })
            .then(issues => { res.send({ errors: [], issues }) });
    });
    // load any additional files you have in routes and apply those to the app
    {
        var fs = require('fs');
        var path = require('path');
        var files = fs.readdirSync("routes");
        for (var index in files) {
            var file = files[index];
            if (file === "index.js") continue;
            // skip non-javascript files
            if (path.extname(file) != ".js") continue;

            var routes = require("./" + path.basename(file));

            if (typeof routes === "function") {
                routes(app, addon);
            }
        }
    }
};
